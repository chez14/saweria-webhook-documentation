# Saweria's Webhook Documentation

This document is not official, so there might be a probability of this
documentation being outdated.

Last Update: see last commit.

## Payload

### Header
- `content-type`: `application/json`
- `saweria-callback-signature`: sha256 hmac with
  `{version}{id}{amount_raw}{donator_name}{donator_email}` for the message and
  your stream key as your secret key. \
  See: [Saweria Signature](README.md#sawerias-signature).

The rest... I think there's nothing special, since it's all barely configured.
Anyway, here's the RAW request from testing.

- `host`: Your hostame, typical http request,
- `content-length`: `312`,
- `user-agent`: `python-requests/2.24.0`,
- `accept-encoding`: `gzip,deflate`,
- `accept`: `*/*`,
- `saweria-callback-signature`: *redacted*,
- `content-type`: `application/json`

### Body
```json
{
    "version": "2022.01",
    "created_at": "2021-01-01T12:00:00+00:00",
    "id": "00000000-0000-0000-0000-000000000000",
    "type": "donation",
    "amount_raw": 69420,
    "cut": 3471,
    "donator_name": "Someguy",
    "donator_email": "someguy@example.com",
    "donator_is_user": false,
    "message": "THIS IS A FAKE MESSAGE! HAVE A GOOD ONE"
}
```

Field description:
- `version` \
    Current version of the payload, this might change overtime.
- `created_at` \
    When was the donation is created (unconfirmed).
- `id` \
    Transaction id (I think, unconfirmed).
- `type` \
    Event type, current known event are just `donation`.
- `amount_raw` \
    Donated amount from the viewer.
- `cut` \
    Fee from QRIS (afaik it's 5%, and for OVO it's 6%).\
    See [Saweria's FAQ](https://saweria.co/faq).
- `donator_*` \
    Information about the donors.
- `message` \
    Message that the dono's send during donation.

## Saweria's Signature

As confirmed via
[Discord](https://discord.com/channels/836767747824156672/836767747824156675/1011068850982354984),
the signature consist from several info you can obtain from the body of the
hook, and then get signed with your stream key with
[HMAC](https://en.wikipedia.org/wiki/HMAC)-SHA256 algorithm.

Message: `{version}{id}{amount_raw}{donator_name}{donator_email}` \
Secret key: *your stream key*

Project/libary that implements this standard:
- https://github.com/SuspiciousLookingOwl/saweria-webhook-express


## Contacts

- Get some help here: \
  https://saweria.co/shortlink

- Or email: \
  <help@saweria.co>